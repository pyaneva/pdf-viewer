Vue.component('pdf', {
      props: ['field']
    })
    
    new Vue({
      el: '#app',
      data: {
        fields: [
           {
             name: 'Link1',
             link: 'http://www.orimi.com/pdf-test.pdf'
           },
           {
             name: 'Link2',
             link: 'https://s1.q4cdn.com/806093406/files/doc_downloads/test.pdf'
           },
           {
             name: 'Link3',
             link: 'http://www.pdf995.com/samples/pdf.pdf'
           },
           {
             name: 'Link4',
             link: 'https://ncu.rcnpv.com.tw/Uploads/20131231103232738561744.pdf'
           }
        ],
        current: 'http://www.orimi.com/pdf-test.pdf',
        current_index: 1
      },
      watch: {
          current_index: function(newv, oldv) {
              this.current = this.fields[newv].link
          }
      },
      mounted () {
      document.addEventListener("keyup", this.nextItem);
      document.addEventListener("keyup", this.prevItem);

      },
      methods: {
        nextItem () {
          if (event.keyCode == 39) {
            this.current_index++
      }
    },
        prevItem () {
          if (event.keyCode == 37) {
            this.current_index--
        }
      },
        changePdf(val, ind) {
          this.current = val.link
          this.current_index = ind
        },
          left: function () {
            if(this.current_index > 0) {
              this.current_index -= 1;
            }
              else {
                this.current_index = this.fields.length-1;
              }
        },
          right: function () {
          
                if(this.current_index < this.fields.length-1) {
                  this.current_index += 1;
                }
                else {
                  this.current_index = 0;
                }
        }
    }
    })
    
